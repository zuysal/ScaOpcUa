/*
 * AdcSampler.cpp
 *
 *  Created on: 1 Mar 2019
 *      Author: pnikiel
 */

#include <AdcSampler.h>
#include <LogIt.h>

namespace Device
{

AdcSampler::AdcSampler(unsigned int maxNumThreads):
        m_runningFlag(false),
        m_requestedNumberThreads(maxNumThreads)
{
    //! Create a list of all AnalogInputSystems declared in this server
    std::transform(
            Device::DRoot::getInstance()->scas().begin(),
            Device::DRoot::getInstance()->scas().end(),
            std::back_inserter(m_analogInputSystems),
            [](DSCA* sca){return sca->analoginputsystem();});
    LOG(Log::INF) << "ADC Sampler: instantiated with " << m_analogInputSystems.size() << " AI Systems";
    m_nextAnalogInputSystem = m_analogInputSystems.begin();
}


AdcSampler::~AdcSampler()
{
}

void AdcSampler::start()
{
    if (m_analogInputSystems.size() < 1)
        return; // nothing to do, do not start any sampler.
    if (m_analogInputSystems.size() < m_requestedNumberThreads)
    {
        LOG(Log::WRN) << "#ADC sampler threads will be " << m_analogInputSystems.size() <<
                " instead of requested " << m_requestedNumberThreads << " to not have more sampling threads than #SCAs...";
        m_requestedNumberThreads = m_analogInputSystems.size();
    }
    m_runningFlag = true;
    for (unsigned int i=0; i<m_requestedNumberThreads; ++i)
        m_threads.emplace_back( &AdcSampler::samplerThread, this );


}


void AdcSampler::stop()
{
    m_runningFlag = false;
    for (std::thread& t : m_threads)
        t.join();
    LOG(Log::INF) << "All ADC sampler threads stopped.";
}

void AdcSampler::samplerThread()
{
    LOG(Log::INF) << "Started ADC sampler thread.";
    while (m_runningFlag)
    {
        decltype(m_nextAnalogInputSystem) iterator;
        {
            // round-robin jump over all declared analog input systems, synchronized between different threads
            std::lock_guard<std::mutex> lock (m_uniqueAccesLock);
            iterator = m_nextAnalogInputSystem;
            m_nextAnalogInputSystem++;
            if (m_nextAnalogInputSystem == m_analogInputSystems.end())
                m_nextAnalogInputSystem = m_analogInputSystems.begin();
        }
        (*iterator)->update();

        usleep (10E3);

    }
}

} /* namespace Device */


