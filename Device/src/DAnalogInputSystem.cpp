
/* This is device body stub */

#include <sstream>
#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DAnalogInputSystem.h>
#include <ASAnalogInputSystem.h>


#include <DAnalogInput.h>

#include <LogIt.h>


namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DAnalogInputSystem::DAnalogInputSystem (
    const Configuration::AnalogInputSystem & config,
    Parent_DAnalogInputSystem * parent
):
    Base_DAnalogInputSystem( config, parent),
	m_sca( parent->sca () ),
	m_currentSourceMask(0)

/* fill up constructor initialization list here */
{
    for (const Configuration::AnalogInput& ai : config.AnalogInput())
    {
        if (ai.enableCurrentSource())
            m_currentSourceMask |= 1 << ai.id();
    }
    LOG(Log::DBG) << "Current source mask for SCA " << getParent()->getFullName() << " was established as: " << std::hex << m_currentSourceMask;

}

/* sample dtr */
DAnalogInputSystem::~DAnalogInputSystem ()
{
}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DAnalogInputSystem::readDiagnostics (
    UaString &value,
    UaDateTime &sourceTime
)
{
	try
	{
		std::stringstream out;
		out << "input line: " << (int)sca()->adc().getInputLine();
		out << " current source mask: 0x" << std::hex << (int)sca()->adc().getCurrentSourceMask();
		out << " gain_calibration: 0x" << std::hex << sca()->adc().getGainCalibration();
		value = UaString(out.str().c_str());
		sourceTime = UaDateTime::now();
		return OpcUa_Good;
	}
	catch (const std::exception &e)
	{
		LOG(Log::ERR) << "Exception: " << e.what();
		return OpcUa_Bad;
	}
}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333


void DAnalogInputSystem::update()
{
    double configuredRefreshRate = getAddressSpaceLink()->getGeneralRefreshRate();
    double refreshPeriodUs = 1000000.0 / configuredRefreshRate;
    if ( std::chrono::duration_cast< std::chrono::microseconds> ( std::chrono::steady_clock::now() - m_lastUpdate  ).count() > refreshPeriodUs )
    {
        m_lastUpdate = std::chrono::steady_clock::now();
        for (auto device : analoginputs())
        {
            device->update();
        }

    }

}

void DAnalogInputSystem::initialize(Sca::Sca& sca)
{
    sca.adc().setCurrentSourceMask(m_currentSourceMask);
}

}

