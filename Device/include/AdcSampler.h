/*
 * AdcSampler.h
 *
 *  Created on: 1 Mar 2019
 *      Author: pnikiel
 */

#ifndef DEVICE_INCLUDE_ADCSAMPLER_H_
#define DEVICE_INCLUDE_ADCSAMPLER_H_

#include <list>
#include <thread>

#include <DSCA.h>
#include <DAnalogInputSystem.h>

namespace Device
{

class AdcSampler
{
public:
    AdcSampler (unsigned int maxNumThreads);
    ~AdcSampler ();

    void start();
    void stop();

    void samplerThread();

private:
    bool                                     m_runningFlag;
    std::list<DAnalogInputSystem*>           m_analogInputSystems;
    decltype(m_analogInputSystems)::iterator m_nextAnalogInputSystem;
    std::list<std::thread>                   m_threads;
    unsigned int                             m_requestedNumberThreads;
    std::mutex                               m_uniqueAccesLock;

};

} /* namespace Device */

#endif /* DEVICE_INCLUDE_ADCSAMPLER_H_ */
