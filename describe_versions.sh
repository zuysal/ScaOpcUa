#!/bin/sh

VERSION_FILE=Server/include/version.h

if [ ! -f $VERSION_FILE ]; then
    echo "You are running this script from wrong working directory"
    exit 1
fi

SCA_OPC_UA=`git describe --tags`
cd ScaSoftware
SCA_SOFTWARE=`git describe --tags`
cd ..
export VERSION_STRING="OpcUaSca: $SCA_OPC_UA ScaSoftware: $SCA_SOFTWARE FELIX=$FELIX"
echo "This will be the version string: '$VERSION_STRING'"
echo "#define VERSION_STR \"$VERSION_STRING\"" > $VERSION_FILE
