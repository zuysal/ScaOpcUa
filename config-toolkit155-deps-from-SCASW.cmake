# LICENSE:
# Copyright (c) 2015, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Author: Piotr Nikiel <piotr@nikiel.info>
# @author pnikiel
# @date 03-Sep-2015
# The purpose of this file is to set default parameters in case no build configuration file (aka toolchain) was specified.

# The approach is to satisfy the requirements as much as possible.

option(HAVE_NETIO "Whether to support netio backend" ON)
option(LOGIT_BACKEND_UATRACE "Whether to build with UATRACE (logging via UASDK logger)" ON)

SET( CMAKE_CXX_COMPILER $ENV{CXX} )
SET( CMAKE_CC_COMPILER $ENV{CC} )

SET( SCASW_LIBS
    $ENV{FELIX_ROOT}/lib/libnetio.so
    $ENV{FELIX_ROOT}/lib/libhdlc_coder.so
    $ENV{FELIX_ROOT}/lib/libtbb.so.2
    $ENV{PROTOBUF_DIRECTORIES}/libprotobuf.so
)

SET( SCASW_LIBDIRS
    $ENV{FELIX_ROOT}/lib
    $ENV{PROTOBUF_DIRECTORIES}
)

SET( SCASW_HEADERS
    $ENV{PROTOBUF_HEADERS}
)

#-------
#Boost - use the one defined by SCA-SW in its setup_paths
#-------
SET( BOOST_PATH_HEADERS $ENV{BOOST_HEADERS} )
SET( BOOST_PATH_LIBS $ENV{BOOST_LIB_DIRECTORIES} )
SET( BOOST_LIBS $ENV{BOOST_LIBS} )

#------
#OPCUA
#------
if(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    message ("Taking OPC UA Toolkit path from the environment: $ENV{OPCUA_TOOLKIT_PATH}")
    SET( OPCUA_TOOLKIT_PATH $ENV{OPCUA_TOOLKIT_PATH} )
else(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    SET( OPCUA_TOOLKIT_PATH "/opt/uasdk-1.5.5-gcc62" )
endif(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    
SET( OPCUA_TOOLKIT_LIBS_DEBUG "-luamodule -lcoremodule -luabase -luastack -luapki -lxmlparser -lxml2 -lssl -lcrypto -lpthread -lrt" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE "-luamodule -lcoremodule -luabase -luastack -luapki -lxmlparser -lxml2 -lssl -lcrypto -lpthread -lrt" ) 

add_custom_target(quasar_opcua_backend_is_ready)

include_directories (
	${OPCUA_TOOLKIT_PATH}/include/uastack
	${OPCUA_TOOLKIT_PATH}/include/uabase
	${OPCUA_TOOLKIT_PATH}/include/uaserver
	${OPCUA_TOOLKIT_PATH}/include/xmlparser
	${OPCUA_TOOLKIT_PATH}/include/uapki
)

#-----
#XML Libs
#-----
#As of 03-Sep-2015 I see no FindXerces or whatever in our Cmake 2.8 installation, so no find_package can be user...
# TODO perhaps also take it from environment if requested

SET( XML_LIBS -lxerces-c  )

#-----
#General settings
#-----

# TODO: split between Win / MSVC, perhaps MSVC has different notation for these
add_definitions(-Wall -DBACKEND_UATOOLKIT -Wno-deprecated -std=gnu++17 -Wno-literal-suffix -Wno-unused-local-typedefs ) 
