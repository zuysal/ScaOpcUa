echo "make_distro: trying to create a package for commit id $1"
DIR=OpcUaScaServer-$1
rm -Rf $DIR $DIR".tar.gz" 
mkdir $DIR 
cp build/bin/OpcUaScaServer bin/ServerConfig.xml bin/config-sim.xml build/Configuration/Configuration.xsd Design/diagram.pdf Documentation/ConfigDocumentation.html Documentation/AddressSpaceDoc.html $DIR || exit 1 
tar cf $DIR".tar" $DIR 
gzip -9 $DIR".tar"  
