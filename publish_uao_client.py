# author: pnikiel
# as per https://its.cern.ch/jira/browse/OPCUA-934

# VERSIONS, etc
DefaultUaoForQuasarVersion = "for-quasar-1.3.4v2"  # should match the tag in UaO repo

import os
import uuid
import shutil
import sys
import glob
import argparse

sys.path.insert(0, './FrameworkInternals')  # this is to let python use quasar's internal code
import manage_files


def copy_multiple(src, dst):
    ''' This function behaves like cp -Rv '''
    for fn in glob.glob( src ):
        print fn
        shutil.copy(fn, '{0}/{1}'.format(dst, os.path.basename(fn)) )
    

#  it is a kerberos address - should be fine for anyone with properly configured CERN machine
TargetGitRepo = "https://:@gitlab.cern.ch:8443/atlas-dcs-opcua-servers/UaoClientForOpcUaSca.git"

TargetGitDir = 'ClientOutput-{0}'.format(str(uuid.uuid4()))

UaoForQuasarGit = "https://github.com/quasar-team/UaoForQuasar.git"

# here we will clone UaoForQuasar - as a subdirectory.
UaoForQuasarDir = 'UaoForQuasar-{0}'.format(str(uuid.uuid4()))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--publish_branch', dest='publish_branch', default='master')
    parser.add_argument('--uao_version', dest='uao_version', default=DefaultUaoForQuasarVersion)
    args = parser.parse_args()
    
    print '--> Cloning UaoForQuasar into {0}'.format(UaoForQuasarDir)
    try:
        err = os.system('git clone {0} {1} -b {2} --depth 1'.format( UaoForQuasarGit, UaoForQuasarDir, args.uao_version ) )
        if err != 0:
            raise Exception('git clone returned non-zero. Look for error above')
        
        sys.path.insert(0, UaoForQuasarDir)
        import generateClass

        allClassesDescriptor =  manage_files.get_list_classes('Design/Design.xml')
        allClasses = map(lambda descriptor: descriptor['name'], allClassesDescriptor )
        print '--> Will generate client code for the following classes: {0}'.format(','.join(allClasses))

        print '--> Using UaoForQuasar to generate all client classes'
        for aClass in allClasses:
            generateClass.runGenerator(aClass,UaoForQuasarDir,namespace='UaoClientForOpcUaSca')

        print '--> You can see this, so Im still alive -- cloning the destination repo'
        if os.system('git clone {0} {1} -b {2}'.format( TargetGitRepo, TargetGitDir, args.publish_branch ) ) != 0:
            raise Exception('Cloning has not been successful.')

        print '--> Removing .orig files ...'
        for fn in glob.glob( '{0}/generated/*.orig'.format(UaoForQuasarDir) ):
            os.remove(fn)
        
        print '--> Copying fresh files'
        copy_multiple( '{0}/generated/*.h'.format(UaoForQuasarDir), '{0}/include'.format(TargetGitDir))
        copy_multiple( '{0}/generated/*.cpp'.format(UaoForQuasarDir), '{0}/src'.format(TargetGitDir))
        copy_multiple( '{0}/include/*.h'.format(UaoForQuasarDir), '{0}/include'.format(TargetGitDir))
        copy_multiple( '{0}/src/*.cpp'.format(UaoForQuasarDir), '{0}/src'.format(TargetGitDir))

        os.chdir( TargetGitDir )
        if os.system('git add * ; git commit -a -m "regenerated" ; git push origin {0}'.format(args.publish_branch)) != 0:
            raise Exception('Your new client has not been pushed... description above')
        os.chdir( '../' )
                
            
    finally:
        if os.path.isdir(UaoForQuasarDir):
            shutil.rmtree( UaoForQuasarDir )
        if os.path.isdir(TargetGitDir):
            shutil.rmtree( TargetGitDir )
                   
if __name__ == "__main__":
    main()

