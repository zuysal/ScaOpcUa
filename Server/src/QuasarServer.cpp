/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QuasarServer.h"
#include <LogIt.h>
#include <string.h>
#include <shutdown.h>
#include <DRoot.h>
#include <DSCA.h>
#include <DAnalogInputSystem.h>
#include <DScaSupervisor.h>
#include <DAdcSampler.h>
#include <DGlobalStatistician.h>

#include <ASNodeQueries.h>
#include <ASMeta.h>
#include <version.h>

#include <ScaSupervisor.h>

#include <ScaCommon/ScaSwLogComponents.h>

#include <boost/foreach.hpp>

QuasarServer::QuasarServer() : BaseQuasarServer()
{
}

QuasarServer::~QuasarServer()
{
    ScaSupervisor::kill();
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
        Device::DRoot::getInstance()->globalstatistician()->update();
    	usleep (100000);
    	Device::DRoot::getInstance()->scasupervisor()->update();
    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server.";

    AddressSpace::ASMeta *meta = AddressSpace::findByStringId<AddressSpace::ASMeta>(m_nodeManager, "Meta");
    if (meta)
    {
        meta->setVersionString(VERSION_STR, OpcUa_Good);
    }
    else
        THROW_WITH_ORIGIN(std::runtime_error, "Logic error. Is your design correct? Meta element seems non existing.");

    ScaSupervisor::instance()->onServerInitialize();

    ScaSupervisor::instance()->printSummary();

    unsigned int adcSamplerThreads = 2; //the default
    if (Device::DRoot::getInstance()->adcsamplers().size()>0)
        adcSamplerThreads = Device::DRoot::getInstance()->adcsamplers()[0]->maxNumberThreads();

    m_adcSampler.reset(new Device::AdcSampler(adcSamplerThreads));
    m_adcSampler->start();
}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
	if (m_adcSampler)
	    m_adcSampler->stop();
	if (ScaSupervisor::getInstance())
	    ScaSupervisor::instance()->stopBackgroundTask();
}

void QuasarServer::initializeLogIt()
{
    BaseQuasarServer::initializeLogIt();
    // here we register additional (i.e. beyond quasar) LogIt components
    Sca::LogComponentLevels::initializeScaSw();
    Log::registerLoggingComponent("Superv", Log::INF);
    Log::registerLoggingComponent("BitBanger", Log::INF);

}
