#!/bin/bash

# A script that setups ground for rpmbuild program.
# It checks out from git, adapts specfile with version information, adjusts ServerConfig.xml version information etc and then runs RPM builder
# Author: Piotr Nikiel, CERN

rm -Rf checkout

REPO_PATH=https://:@gitlab.cern.ch:8443/atlas-dcs-opcua-servers/ScaOpcUa.git

if [ $# -ne 2 ]; then
	echo "Usage: "
	echo "./buildRpm.sh --tag <tag name only numbers eg 1.2.3-4 >"
	echo "or"
	echo "./buildRpm.sh --branch <git branch>"
	echo "Note: building from branches is only for development, because it is not uniquely identifiable. For distribution and production always build from tag."
	exit -1
elif [ $1 = "--tag" ]; then
	echo "Building from tag: $2"
	TAG=$2
	TAG_ARRAY=(${TAG//-/ })
	RPM_VERSION=${TAG_ARRAY[0]}
	RPM_RELEASE=${TAG_ARRAY[1]}
elif [ $1 = "--branch" ]; then
	echo "Building from branch: $2"
	RPM_VERSION=$( echo "$2" | tr - _)  # replace dashes with undescrores (dashes are illegal in RPM version nmbers)
	RPM_RELEASE=0
else
	echo "--tag or --branch only allowed"
fi

if [ -z "$RPM_RELEASE" ]; then
    RPM_RELEASE="0"
fi 


git clone $REPO_PATH --depth 1 --recursive -b $2  checkout || exit 1
echo "Cloned and checked out, now adapting version strings etc"
cd checkout
source describe_versions.sh
VERSION_STR="$VERSION_STRING\nBuilt by quasar RPM builder for git, args given: $1 $2"
VERSION_ADDITIONAL="\nBuilt by: $USER on `date` on $HOSTNAME arch=`arch` "
VERSION_STR="$VERSION_STR $VERSION_ADDITIONAL"
cd ..
echo "#define VERSION_STR \"$VERSION_STR\"" > checkout/Server/include/version.h
SED_EXPRESSION="s@<SoftwareVersion>[0-9.a-zA-Z]*<\/SoftwareVersion>@<SoftwareVersion>$2<\/SoftwareVersion>@"
echo "Invoking sed with expression: $SED_EXPRESSION"
sed $SED_EXPRESSION checkout/bin/ServerConfig.xml > checkout/bin/ServerConfig.xml.new || exit 1
rm checkout/bin/ServerConfig.xml
mv checkout/bin/ServerConfig.xml.new checkout/bin/ServerConfig.xml
rm -Rf rpmbuild
mkdir rpmbuild rpmbuild/SOURCES rpmbuild/SPECS rpmbuild/BUILD rpmbuild/BUILDROOT rpmbuild/RPMS
tar cf rpmbuild/SOURCES/checkout.tar checkout/
gzip -1 rpmbuild/SOURCES/checkout.tar 
echo "%define version $RPM_VERSION" > rpmbuild/SPECS/spec.spec
echo "%define release $RPM_RELEASE" >> rpmbuild/SPECS/spec.spec
cat checkout/RPM/build_from_git/template.spec >> rpmbuild/SPECS/spec.spec
rm -Rf checkout
cd rpmbuild
rpmbuild -bb SPECS/spec.spec || exit 1
echo "rpmbuild finished OK, your rpm should be here: `pwd`/RPMS/x86_64/"

