#!/bin/bash

# Generate protocol buffers for the SCA OPC UA server
# Protocol buffers are used in the GPIO Bit Banger
#
# 


echo "Generating BitBang protocol buffers..."

protoc --cpp_out=./ BitBangProtocol.proto
mv BitBangProtocol.pb.h ../Device/include/
mv BitBangProtocol.pb.cc ../Device/src/
